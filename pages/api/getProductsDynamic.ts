// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { groq } from "next-sanity";
import { sanityClient } from "../../sanity";

// const categoryName = "Chronograph First";
// const colour = "Cream";

// // const query = groq`*[_type == "product" && "Chronograph First" in category->{title}] {

// const slug = "chronograph-first/black"
// const query =
//   // groq`*[_type == "product" && category->title=="${categoryName}" && colour=="${colour}"] {

//   groq`*[_type == "product" && slug.current=="${slug}"] {
//     _id,
//   "category":category->title,
//   // category,
//   title,
//   colour,
//   sku,
//   price,
//   body{
//     en[]{
//       children[]{
//         text
//       }

//     }
//   },
//   blurb,
//   slug,
//   // "slug":slug.current,
//   images[]{
//     asset->{
//       url
//     }
//   },
//   gallery[]{
//     asset->{
//       url
//     }
//   },
//   tags[]
//   }
//   `;

// // export const queryParams = {slug :query.slug}
// // const params = { categoryName };

// type Data = {
//   products: Product[];
// };

// export default async function handler(
//   req: NextApiRequest,
//   res: NextApiResponse<Data>
// ) {
//   const products: Product[] = await sanityClient.fetch(query);
//   console.log("getProductsDynamic.ts Fetch: products[] length:", products.length);
//   // console.log(products);
//   res.status(200).json({ products });
// }

//{
//   if (req.method !== 'GET') {
//     res.setHeader('Allow', ['GET']);
//     res.status(405).json({ products: `Method ${req.method} is not allowed` });
//   } else {
//   const products: Product[] = await sanityClient.fetch(query);
//   console.log("getProductsDynamic.ts Fetch: products[] length:", products.length);
//   // console.log(products);
//   res.status(200).json({ products });
// }
// }


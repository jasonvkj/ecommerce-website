// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { groq } from "next-sanity";
import { sanityClient } from "../../sanity";

// const query = groq`*[_type == "product" && "Chronograph First" in category->{title}] {
const query = groq`*[_type == "category"] {
    _id,
      title,
      sku,
      price,
      slug,
      images[]{
        asset->{
          url
        }
      },
      } | order(sku asc)
      `;

type Data = {
  categories: Category[];
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const categories: Category[] = await sanityClient.fetch(query);
  // console.log(categories);
  console.log(categories.length);
  res.status(200).json({ categories });
}

// // const query = groq`*[_type == "product" && "Chronograph First" in category->{title}] {
// const query = groq`*[_type == "category"] {
// _id,
//   title,
//   sku,
//   price,
//   slug,
//   images[]{
//     asset->{
//       url
//     }
//   },
//   } | order(sku asc)
//   `;

// type Data = {
//   categories: Category[];
// };

// export default async function handler(
//   req: NextApiRequest,
//   res: NextApiResponse<Data>
// ) {
//   const categories: Category[] = await sanityClient.fetch(query);
//   // console.log(categories);
//   console.log(categories.length);
//   res.status(200).json({ categories });
// }

// Get products with groq
// const query =groq`*[_type == "product"] {
// _id,
// "category":category->title,
// title,
// colour,
// sku,
// price,
// body{
//   en[]{
//     children[]{
//       text
//     }

//   }
// },
// blurb,
// slug,
// images[]{
//   asset->{
//     url
//   }
// },
// tags[]
// }
// `

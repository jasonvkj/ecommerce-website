// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { groq } from "next-sanity";
import { sanityClient } from "../../sanity";

const query = groq`*[_type == "product"] {
_id,
"category":category->title,
  title,
  colour,
  sku,
  price,
  body{
    en[]{
      children[]{
        text
      }

    }
  },
  blurb,
  slug,
  images[]{
    asset->{
      url
    }
  },
  tags[]
} | order(title asc)`;

type Data = {
  products: Product[];
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const products: Product[] = await sanityClient.fetch(query);
  console.log(products);
  res.status(200).json({ products });
}

// Get products with groq
// const query = groq`*[_type == "product"] {
//   _id,
//     title,
//     price,
//     sku,
//     slug,
//     categories,[]->{
//       title
//     }
//     images[]{
//        asset->{url}
//   }`;

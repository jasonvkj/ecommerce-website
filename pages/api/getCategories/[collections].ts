import { NextApiRequest, NextApiResponse } from "next";
import { groq } from "next-sanity";
import { sanityClient } from "../../../sanity";

type Data = {
  products: Product[];
};

export default async function dynamicProductHandler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  console.log("response on server: ", req.body, req.query, req.cookies); // request body

  const {collections} = req.query;
  const products: Product[] = await sanityClient.fetch(
    // groq`*[_type == "product" && category->title=="${categoryName}" && colour=="${colour}"] {

    // groq`*[_type == "product" && category->title=="${categoryName}"] {

    // Work out which field to filter products
    // groq`*[_type == "product" && category->title=="${category}"] {
    // groq`*[_type == "product" && "BP002" in sku] {

    groq`*[_type == "product" && slug.current match "${collections}"] {
    
    _id,
  "collection":category->title,
  "caseMaterial":category->caseMaterial,
//   category,
  title,
  colour,
  sku,
  price,
  body{
    en[]{
      children[]{
        text
      }

    }
  },
  blurb,
  slug,
  // "slug":slug.current,
  images[]{
    asset->{
      url
    }
  },
  tags[]
  }
  `
  );
  // .then(res=> res.json({ products }));

  // const products: Product[] = await sanityClient.fetch(query, {colour});

  // res.end(`Post: ${colour}`);

  console.log(
    "getCategory/[collections].ts Fetch: products[] length:",
    products.length
  );
  console.log("typeof products:", typeof products);
  res.status(200).json({ products });
  // return products
  // return res
}

import { NextApiRequest, NextApiResponse } from "next";
import { groq } from "next-sanity";
import { sanityClient } from "../../../../sanity";

type Data = {
  products: Product[];
};

export default async function dynamicProductHandler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  console.log("response on server: ", req.body, req.query, req.cookies); // request body

  const { colour, collections } = req.query;
  const products: Product[] = await sanityClient.fetch(
    // groq`*[_type == "product" && category->title=="${categoryName}" && colour=="${colour}"] {

    // groq`*[_type == "product" && category->title=="${categoryName}"] {
    groq`*[_type == "product" && slug.current match "${collections}" && colour=="${colour}"] {
      _id,
      "functions":category->functions,
      "category":category->title,
      "caseMaterial":category->caseMaterial,
      "caseDiameter":category->caseDiameter,
      "caseHeight":category->caseHeight,
      "caseWaterRest":category->caseWaterRest,
      "caseFeats":category->caseFeats,
      "dialFinish":category->dialFinish,
      "dialLume":category->dialLume,
      "dialIndex":category->dialIndex,
      "calibreName":category->calibreName,
      "calibreModel":category->calibreModel,
      "calibreBatteryLife":category->calibreBatteryLife,
      "calibreAccuracy":category->calibreAccuracy,
      "calibreJewels":category->calibreJewels,
      "lensFinish":category->lensFinish,
      "lensMaterial":category->lensMaterial,
      "strapName":category->strapName,
      "strapStyle":category->strapStyle,
      // category,
      title,
      colour,
      sku,
      price,
      "body":body.en[].children[]{_key,"description":text},

      // "description":body.en[].children[],
      
      // "body2":body->children[],
      blurb,
      slug,
      // "slug":slug.current,
      images[]{
        asset->{
          url
        }
    },
    gallery[]{
      asset->{
        url
      }
    },
    tags[]
  }
  `
  );
  // .then(res=> res.json({ products }));

  // const products: Product[] = await sanityClient.fetch(query, {colour});

  // res.end(`Post: ${colour}`);

  console.log(
    "getCategories/[collection]/[colour].ts Fetch: products[] length:",
    products.length
  );
  console.log("typeof products:", typeof products);
  res.status(200).json({ products });
  
  

  // try {
  //     res.status(200)?res.json({ products }):res.status;

  // }
  // catch (err) {
    
  // }
  // return res.json


  // return products
  // return res
}

// Original copy
// const query =
//   // groq`*[_type == "product" && category->title=="${categoryName}" && colour=="${colour}"] {

//   // groq`*[_type == "product" && category->title=="${categoryName}"] {
//   groq`*[_type == "product" && category->title=="Chronograph First" && colour=="${colour}"] {
//       _id,
//   "category":category->title,
//   // category,
//   title,
//   colour,
//   sku,
//   price,
//   body{
//     en[]{
//       children[]{
//         text
//       }

//     }
//   },
//   blurb,
//   slug,
//   // "slug":slug.current,
//   images[]{
//     asset->{
//       url
//     }
//   },
//   tags[]
//   }
//   `;

// type Data = {
//   products: Product[];
// };

// export default async function dynamicProductHandler(
//   req: NextApiRequest,
//   res: NextApiResponse<Data>
// ) {
//   const { colour } = req.query;
//   const products: Product[] = await sanityClient.fetch(query, {colour});

//   res.end(`Post: ${colour}`)

//   console.log(
//     "getProductsByCategory/[colour].ts Fetch: products[] length:",
//     products.length
//   );
//   // console.log(products);
//   res.status(200).json({ products });
// }

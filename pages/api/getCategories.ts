// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { groq } from "next-sanity";
import { sanityClient } from "../../sanity";
import { Category } from "../../src/types";

const query = groq`*[_type == "category"] {
  _id,
    title,
    sku,
    price,
    slug,
  // "slug":slug.current,
    images[]{
      asset->{
        url
      }
    },
    functions,
    caseMaterial,
    caseDiameter,
    caseHeight,
    caseWaterRest,
    caseFeats,
    dialFinish,
    dialLume,
    dialIndex,
    calibreModel,
    calibreBatteryLife,
    calibreAccuracy,
    lensFinish,
    lensMaterial,
    strapName,
    strapStyle,
    } | order(sku asc)
    `;

type Data = {
  categories: Category[];
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const categories: Category[] = await sanityClient.fetch(query);
  // console.log(categories);
  console.log(categories.length);
  res.status(200).json({ categories });
}

// const query = groq`*[_type == "category"] {
// _id,
//   ...
// }| order(sku asc)`;

// type Data = {
//   categories: Category[];
// };

// export default async function handler(
//   req: NextApiRequest,
//   res: NextApiResponse<Data>
// ) {
//   const categories = await sanityClient.fetch(query);
//   console.log(categories);
//   res.status(200).json({ categories });
// }

// *[_type == "category"] {
//     _id,
//      title,
//      slug,
//      images[]{
//        asset->{url}
//      }
//     }

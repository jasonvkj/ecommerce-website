import "../src/styles/globals.css";

import type { AppProps } from "next/app";
import Header from "../src/components/Header";
import Footer from "../src/components/Footer";
import { Toaster } from "react-hot-toast";
import { SessionProvider } from "next-auth/react";
import { CartProvider } from "../src/CartContext";

function MyApp({ Component, pageProps }: AppProps) {
  return (

    <SessionProvider session={pageProps.session}>
      <Header />
      <CartProvider>

        <Component {...pageProps} />

      </CartProvider>
      <Toaster position="bottom-center" reverseOrder={false} />
      <Footer />
    </SessionProvider>

  );
}

export default MyApp;

import { GetStaticProps } from "next";
import Link from "next/link";
import Image from "next/legacy/image";

import { useRouter } from "next/router";
import React, { useState } from "react";
import { fetchProductsByCategory } from "../../src/utils/fetchProductsByCategory";

import styles from "../../src/styles/Collections.module.css";
import { urlFor } from "../../sanity";
import  Head  from "next/head";

// const abs = "s";

interface Props {
  categories: Category[];
}

const Collections = ({ categories }: Props) => {
  const [isClicked, setIsClicked] = useState(Boolean);

  const router = useRouter();
  const { slug } = router.query;
  console.log("pages/collections/index.tsx loaded", '\n',
    "Slug fetched and processed with useRouter in top line: ",
    router, slug
  );


  return (

    <div
      id="collections"
      className="h-full pt-[7em] md:h-full md:pt-[10em] flex justify-center items-center"
    >
      <Head>
        <title>Our Collections</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="a">
        <div className="grid md:grid-cols-2 lg:grid-cols-3 mx-5">
          {/* <div>{categories[0].title}</div> */}
          {/* INSERT COLLECTIONS MAPPING HERE */}

          {categories.map((category, index) => {
            return (
              <div
                className="mx-2  mb-5 pt-1 flex flex-col justify-center items-center"
                key={index}
              >
                <Link
                  className="cursor-pointer"
                  href={`collections/${category.slug.current}`}
                  passHref
                >
                  <div className="bg-zinc-200 p-2">
                    <Image
                      src={urlFor(category.images[0].asset.url).url()}
                      alt="watch-template"
                      width="500"
                      height="500"
                      className={styles.watchTemplate}
                      objectFit="contain"
                    />
                  </div>
                  <div className={`mt-3 text-center ${styles.itemTitle}`}>
                    {category.title.toUpperCase()}
                  </div>
                  {/* <div className={`mt-3 text-center  ${styles.price}`}>
                      HK$ {category.price.toLocaleString()}
                    </div> */}
                </Link>
              </div>
            );
          })} </div>
      </div>
    </div>

  );
};

export default Collections;


// export const getServerSideProps: GetServerSideProps<Props> = async () => {
export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  // const pid = params;

  const categories = await fetchProductsByCategory();
  //console.log(categories);
  return {
    props: {
      categories,
      // products,
      // session,
    },
  };
};

import { GetServerSideProps } from "next";
import Link from "next/link";
import Head from "next/head";

import Image from "next/legacy/image";

import { useRouter } from "next/router";
import React, { useEffect } from "react";

import styles from "../../src/styles/Collections.module.css";
import { urlFor } from "../../sanity";

interface Props {
  //   categories: Category[];
  products: Product[];
}



const Collections = ({ products }: Props) => {
  const router = useRouter();
  const { slug } = router.query;
  console.log("pages/collections/[collection].tsx loaded", '\n',
    "Slug fetched and processed with useRouter in top line: ",
    router,
    slug
  );

  useEffect(() => {
    console.log("useEffect() -> router.query: ", router.query);
  }, [router.query]);

  return (
    <div
      id="collections"
      className="h-full pt-[7em] md:h-full md:pt-[10em] flex justify-center items-center"
    >
      <Head>
        <title>{products[0].collection.toString()}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="a">
        <div className="grid md:grid-cols-2 lg:grid-cols-3 mx-5">
          {/* <div>{categories[0].title}</div> */}
          {/* INSERT COLLECTIONS MAPPING HERE */}

          {products.map((product, index) => {
            return (
              <div
                className="mx-2  mb-5 pt-1 flex flex-col justify-center items-center"
                key={index}
              >
                <Link
                  className="cursor-pointer"
                  href={`/${product.slug.current}`}
                  passHref
                >
                  <div className="bg-zinc-200 p-2">
                    {/* {category.images[0].asset.url} */}

                    <Image
                      src={urlFor(products[index].images[0]?.asset.url).url()}
                      alt="watch-template"
                      width="500"
                      height="500"
                      className={styles.watchTemplate}
                      objectFit="contain"
                    />
                  </div>
                  <div className={`mt-3 text-center ${styles.itemTitle}`}>
                    {String(product.collection).toUpperCase()}
                  </div>
                  <div className={` text-center ${styles.subTitle}`}>
                    {product.colour.replaceAll('-', ' ').toUpperCase()}
                  </div>
                  <div className={`mt-3 text-center  ${styles.price}`}>
                    HK$ {product.price.toLocaleString()}
                  </div>
                </Link>
              </div>
            );
          })}
        </div>
      </div>
    </div>

  );
};

export default Collections;

export const getServerSideProps: GetServerSideProps<Props> = async (params) => {
  // export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  // const pid = params;

  const collections = params.query?.collections; // GetServerSideProps
  console.log("params.query in GSSP: ", collections);

  const res = await fetch(
    `${process.env.NEXT_PUBLIC_BASE_URL}/api/getCategories/${collections}` // TESTING
  );

  //   const categories = await fetchCategories();
  //   const products = await fetchProducts();

  console.log(res);
  const data = await res.json();
  console.log(data);
  const products: Product[] = data.products;

  console.log(products);
  return {
    props: {
      //   categories,
      products,
      // session,
    },
  };
};

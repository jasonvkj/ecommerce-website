import React, { useEffect } from "react";
import Image from "next/legacy/image";
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Pagination, A11y } from "swiper";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import styles from "../../src/styles/Products.module.css";

import CFOutline from "../../public/CF_Outline.svg";
import { urlFor } from "../../sanity";
import { GetServerSideProps } from "next";
import { useRouter } from "next/router";
import { Disclosure, DisclosureButton, DisclosurePanel } from "@headlessui/react";
import { MinusIcon, PlusIcon } from "@heroicons/react/20/solid";
import Head from "next/head";
import { useCart } from "../../src/CartContext";
import { minusButton, plusButton } from "../../src/components/Images";
import coverImg from "../../public/bg-landing.jpg"


interface Props {
  products: Product[];
}

interface slug {
  pathname: string;
  route: string;
  query: string;
  asPath: string;
}

function Colour({ products }: Props) {
  const router = useRouter();
  // const slug = router.route.slice(1);
  const { slug } = router.query;

  // const query = router.query;
  console.log("pages/[collections]/[colour].tsx loaded", '\n',
    "Slug fetched and processed with useRouter in top line: ",
    router,
    slug
  );

  const [count, setCount] = React.useState(1);
  const decrementCounter = () => {
    // setCount(prevCount => ({value: Math.max(prevCount.value - 1, 0)}));
    setCount((prevCount) => Math.max(prevCount - 1, 0));
  };

  const incrementCounter = () => {
    setCount((prevCount) => prevCount + 1);
  };

  const { addToCart } = useCart();

  const handleAddToCartClick = (products: Product[]) => {
    addToCart(products);
    console.log("handleAddToCartClick clicked");
  };


  //Client side rendering functions
  useEffect(() => {
    console.log("useEffect() -> router.query: ", router.query);
  }, [router.query]);

  return (
    <>
      <Head>
        <title>{products[0].title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="">
        <Image src={coverImg} alt="Cover Image" className={` ${styles.coverImg}`}
          objectFit="cover" layout="fill"
        />
      </div>


      <div
        id="collections"
        className={`h-full md:h-full md:pt-[6em] md:pb-[3em] flex flex-wrap justify-around items-center bg-[#e1dbd200] ${styles.section1}`}
      >



        {/* Other main content divs children here */}



        <div className="h-full md:w-[30em] w-full">
          <div className="swiper-container h-full w-full">
            <Swiper modules={[Navigation, Pagination, A11y]}
              spaceBetween={50}
              slidesPerView={1}
              // navigation
              pagination={{ clickable: true }}
              onSlideChange={() => console.log("slide change")}
              onSwiper={(swiper) => console.log(swiper)}
              className="grid ">
              {(products[0].images.map((image, index) => {
                return (
                  <SwiperSlide className="" key={index}>
                    <div className="z-8 m-10 md:pt-5 mb-[10em] md:w-[25em] h-[25em] flex flex-col  text-[#4a3826]">

                    </div>
                    <Image
                      src={urlFor(products[0].images[index].asset.url)
                        .quality(75)
                        .url()} // ADJUST SIZE OR QUALITY TO FASTER LOADING
                      alt="chrono-parallax"
                      // width="w-full"
                      // height="500"
                      className={styles.bgWrapper}
                      objectFit="contain"
                      objectPosition="center"
                      layout="fill"
                    ></Image>
                  </SwiperSlide>
                );
              }))}
            </Swiper>
          </div>
        </div>
        <div className="flex flex-wrap md:flex-nowrap">
          {/* <div className="flex ">
            
            <Image
              priority
              src={urlFor(products[0].images[0].asset.url).url()}
              alt="watch-template"
              width="500"
              height="500"
              className={styles.watchTemplate}
              objectFit="contain"
            ></Image>

          </div> */}



          <div className="text-start flex flex-col pl-6 pr-6 bg-white md:bg-[#e1dbd2]   ">
            {/* <p className={`${styles.itemTitle}`}>Chronograph First</p> */}
            {/* <p className={`${styles.itemTitle}`}>{products[0].category}</p> */}
            <p className={`${styles.itemTitle}`}>
              {/* {products[0].title.toUpperCase()} */}
              {products[0].category.toString().toUpperCase()}
            </p>
            <p className={`${styles.itemSubTitle}`}>
              {products[0].colour.toUpperCase()}
            </p>

            {/* <p className={`${styles.itemSubTitle}`}>{products[0].colour}</p> */}
            <p className={`${styles.price}`}>
              HK${products[0].price.toLocaleString()}
            </p>
            {/* <p className={`${styles.itemDesc}`}>
              Designed for any situations, the Burlap completes your outfit with
              a tasteful vintage touch.
            </p> */}

            <div className={`  md:inline-block`}>
              {products[0].body.map((desc, index) => {
                return (
                  <p key={index} className={`${styles.itemDesc}`}>
                    {products[0].body[index].description}
                  </p>
                );
              })}
            </div>

            {/* <p className={`${styles.itemDesc}`}>
              May this statement piece inspire perseverance in what you love.
            </p> */}

            <div className="addToCartContainer flex flex-col justify-between items-center md:pt-10">
              <div
                className={`flex justify-around items-center ${styles.counterContainer}`}
              >
                <div className=" ">
                  <button
                    className="flex justify-center items-center h-full w-[2em]"
                    onClick={decrementCounter}
                  >
                    {minusButton()}
                  </button>
                </div>
                <div className={styles.counter}>{count}</div>
                <div className="">
                  <button
                    className="flex justify-center items-center h-[2em] w-[2em]"
                    onClick={incrementCounter}
                  >
                    {plusButton()}
                  </button>
                </div>
              </div>

              <div className={styles.orderButton}>
                <button className="flex justify-center items-center h-full w-full" onClick={(e) => {
                  e.preventDefault();
                  handleAddToCartClick(products)
                }}>
                  ADD TO CART
                </button>

              </div>

              {/* <div>
                <button onClick={() => setCount(1)}>RESET</button>
              </div> */}



            </div>


          </div>
        </div>
      </div >

      {/*     TECHNICAL DETAILS       */}

      <div className="specifications py-5 bg-[#eeeeee]" >
        <div className="grid grid-flow-cols-2 md:grid-flow-cols-2">
          <div className="grid justify-items-center">
            {products[0].images[1] ? (
              <Image
                src={urlFor(products[0].images[1].asset.url).url()}
                alt="technical-details"
                width="400"
                height="400"
                className={`bg-[#e7e9ea] ${styles.watchTemplate}`}
                objectFit="cover"
              ></Image>
            ) : (
              <Image
                src={CFOutline}
                alt="CFOutline"
                width="400"
                height="400"
                objectFit="contain"
              ></Image>
            )}
          </div>
          <div>
            <p className={`grid justify-items-center ${styles.specTitle}`}>
              TECHNICAL DETAILS
            </p>
            <p
              className={`grid justify-items-center pt-0 text-[1.25em] text-[#4a3826]`}
            >
              - {products[0].sku} -
            </p>

            {/* <ChronoFirstSpecs /> */}
            <div className="w-full pt-5 flex justify-center ">
              <div className=" w-full max-w-md rounded-2xl bg-[#eeeeee] p-2">
                <Disclosure>
                  {({ open }) => (
                    <>
                      <DisclosureButton className="flex items-center w-full justify-between rounded-lg border-b-2  bg-[#eeeeee] py-2 px-2 text-left text-sm md:text-lg font-medium text-[#4a3826] hover:[#e1dbd2] focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                        <span>Functions</span>

                        {open ? (
                          <MinusIcon className=" h-5 w-5 text-[#4a3826]" />
                        ) : (
                          <PlusIcon className="h-5 w-5 text-[#4a3826]" />
                        )}
                      </DisclosureButton>
                      <DisclosurePanel className="px-4 pt-4  text-sm text-gray-500">
                        <div className="">
                          {/* <p className={`pb-3 ${styles.specSubtitle}`}>Functions</p> */}
                          <p className={`flex flex-col ${styles.specDesc}`}>
                            {products[0].functions}
                          </p>

                          {/* {products[0].functions.map((func, index) => {
                            return (
                              <p key={index} className={`${styles.specDesc}`}>
                                {products[0].func}
                              </p>
                            );
                          })} */}

                          {/* <p className={styles.specDesc}>Chronograph</p>
                          <p className={styles.specDesc}>Date</p> */}
                        </div>
                      </DisclosurePanel>
                    </>
                  )}
                </Disclosure>
                <Disclosure>
                  {({ open }) => (
                    <>
                      <DisclosureButton className="flex items-center w-full justify-between rounded-lg border-b-2  bg-[#eeeeee] py-2 px-2 text-left text-sm md:text-lg font-medium text-[#4a3826] hover:[#e1dbd2] focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                        <span>Case</span>
                        {open ? (
                          <MinusIcon className="h-5 w-5 text-[#4a3826]" />
                        ) : (
                          <PlusIcon className="h-5 w-5 text-[#4a3826]" />
                        )}
                      </DisclosureButton>
                      <DisclosurePanel className="px-4 pt-4  text-sm text-gray-500">
                        <div className="">
                          <p className={styles.specSubtitle}>
                            {products[0].caseMaterial}
                          </p>
                          <p className={styles.specDesc}>Case Material</p>
                          <p className={styles.specSubtitle}>
                            {products[0].caseDiameter}
                          </p>
                          <p className={styles.specDesc}>Case Diameter</p>
                          <p className={styles.specSubtitle}>
                            {products[0].caseHeight}
                          </p>
                          <p className={styles.specDesc}>Case height</p>
                          <p className={styles.specSubtitle}>
                            {products[0].caseWaterRest}
                          </p>
                          <p className={styles.specDesc}>Water resistance</p>
                          <p className={styles.specSubtitle}>Features</p>
                          <p className={styles.specDesc}>
                            {products[0].caseFeats}
                          </p>
                          {/* <p className={styles.specDesc}>7 mm crown</p> */}
                        </div>
                      </DisclosurePanel>
                    </>
                  )}
                </Disclosure>
                <Disclosure as="div" className="mt-2">
                  {({ open }) => (
                    <>
                      <DisclosureButton className="flex items-center w-full justify-between rounded-lg border-b-2  bg-[#eeeeee] py-2 px-2 text-left text-sm md:text-lg font-medium text-[#4a3826] hover:[#e1dbd2] focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                        <span>Dial</span>
                        {open ? (
                          <MinusIcon className="h-5 w-5 text-[#4a3826]" />
                        ) : (
                          <PlusIcon className="h-5 w-5 text-[#4a3826]" />
                        )}
                      </DisclosureButton>
                      <DisclosurePanel className="px-4 pt-4  text-sm text-gray-500">
                        <div className="">
                          <p className={styles.specSubtitle}>
                            {products[0].dialFinish}
                          </p>
                          <p className={styles.specDesc}>Finish</p>
                          <p className={styles.specSubtitle}>
                            {products[0].dialLume}
                          </p>
                          <p className={styles.specDesc}>Luminous material</p>
                          <p className={styles.specSubtitle}>
                            {products[0].dialIndex}
                          </p>
                          <p className={styles.specDesc}>Embossment</p>
                        </div>
                      </DisclosurePanel>
                    </>
                  )}
                </Disclosure>
                <Disclosure>
                  {({ open }) => (
                    <>
                      <DisclosureButton className="flex items-center w-full justify-between rounded-lg border-b-2  bg-[#eeeeee] py-2 px-2 text-left text-sm md:text-lg font-medium text-[#4a3826] hover:[#e1dbd2] focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                        <span>Calibre</span>
                        {open ? (
                          <MinusIcon className="h-5 w-5 text-[#4a3826]" />
                        ) : (
                          <PlusIcon className="h-5 w-5 text-[#4a3826]" />
                        )}
                      </DisclosureButton>
                      <DisclosurePanel className="px-4 pt-4 text-sm text-gray-500">
                        <div className="">
                          <p className={styles.specSubtitle}>
                            {products[0].calibreName}
                          </p>
                          <p className={styles.specDesc}>
                            {products[0].calibreModel}
                          </p>
                          <p className={styles.specSubtitle}>
                            {products[0].calibreBatteryLife}
                          </p>
                          <p className={styles.specDesc}>Battery life</p>
                          <p className={styles.specSubtitle}>
                            {products[0].calibreAccuracy}
                          </p>
                          <p className={styles.specDesc}>Accuracy</p>
                          {products[0].calibreJewels ? (
                            <>
                              <p className={styles.specSubtitle}>
                                {products[0].calibreJewels}
                              </p>
                              <p className={styles.specDesc}>Jewels</p>
                            </>
                          ) : (
                            <></>
                          )}
                        </div>
                      </DisclosurePanel>
                    </>
                  )}
                </Disclosure>
                <Disclosure>
                  {({ open }) => (
                    <>
                      <Disclosure.Button className="flex items-center w-full justify-between rounded-lg border-b-2  bg-[#eeeeee] py-2 px-2 text-left text-sm md:text-lg font-medium text-[#4a3826] hover:[#e1dbd2] focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                        <span>Lens</span>
                        {open ? (
                          <MinusIcon className="h-5 w-5 text-[#4a3826]" />
                        ) : (
                          <PlusIcon className="h-5 w-5 text-[#4a3826]" />
                        )}
                      </Disclosure.Button>
                      <Disclosure.Panel className="px-4 pt-4 text-sm text-gray-500">
                        <div className="">
                          <p className={styles.specSubtitle}>
                            {products[0].lensFinish}
                          </p>
                          <p className={styles.specDesc}>
                            {products[0].lensMaterial}
                          </p>
                        </div>
                      </Disclosure.Panel>
                    </>
                  )}
                </Disclosure>
                <Disclosure>
                  {({ open }) => (
                    <>
                      <DisclosureButton className="flex items-center w-full justify-between rounded-lg border-b-2  bg-[#eeeeee] py-2 px-2 text-left text-sm md:text-lg font-medium text-[#4a3826] hover:[#e1dbd2] focus:outline-none focus-visible:ring focus-visible:ring-purple-500 focus-visible:ring-opacity-75">
                        <span>Strap</span>
                        {open ? (
                          <MinusIcon className="h-5 w-5 text-[#4a3826]" />
                        ) : (
                          <PlusIcon className="h-5 w-5 text-[#4a3826]" />
                        )}
                      </DisclosureButton>
                      <DisclosurePanel className="px-4 pt-4  text-sm text-gray-500">
                        <div className="">
                          <p className={styles.specSubtitle}>
                            {products[0].strapName}
                          </p>
                          <p className={styles.specDesc}>
                            {products[0].strapStyle}
                          </p>
                        </div>
                      </DisclosurePanel>
                    </>
                  )}
                </Disclosure>
              </div>
            </div>
          </div>
        </div>
      </div >

      <div className="flex flex-col justify-center items-center h-[10em] bg-[#979797]">
        <div className="">
          <p className={styles.logoTitle}>CRAFTED TO PERFECTION</p>
        </div>
      </div>

      {/* GALLERY IMAGES */}

      <div className="swiper-container h-full w-full">
        <Swiper
          modules={[Navigation, Pagination, A11y]}
          spaceBetween={50}
          slidesPerView={1}
          // navigation
          pagination={{ clickable: true }}
          onSlideChange={() => console.log("slide change")}
          onSwiper={(swiper) => console.log(swiper)}
          className="grid "
        >
          {(products[0].gallery) ? (products[0].gallery.map((image, index) => {
            return (
              <SwiperSlide className="" key={index}>
                <div className="z-8 m-10 md:pt-5 mb-[10em] md:w-[25em] h-[25em] flex flex-col  text-[#4a3826]">
                  {/* <div className="rounded-md bg-[#e1dbd2c2]">
                    <h1
                      className={`flex justify-center p-5 text-xl md:text-2xl font-bold ${styles.slogan}`}
                    >
                      BACK TO THE ROOTS
                    </h1>
                    <p
                      className={`px-5 pb-5 text-md flex justify-center text-center ${styles.text}`}
                    >
                      All our collections are proudly designed and assembled in
                      Hong Kong.
                    </p>
                    <p
                      className={`px-5 pb-5 text-md hidden md:flex justify-center text-center ${styles.text}`}
                    >
                      And so, all are marked with <br /> &quot;BUILT IN HONG
                      KONG&quot;.
                    </p>
                  </div> */}
                </div>
                <Image
                  src={urlFor(products[0].gallery[index].asset.url)
                    .quality(75)
                    .url()} // ADJUST SIZE OR QUALITY TO FASTER LOADING
                  alt="chrono-parallax"
                  // width="w-full"
                  // height="500"
                  className={styles.bgWrapper}
                  objectFit="cover"
                  objectPosition="center"
                  layout="fill"
                ></Image>
              </SwiperSlide>
            );
          })) : (products[0].images.map((image, index) => {
            return (
              <SwiperSlide className="" key={index}>
                <div className="z-8 m-10 md:pt-5 mb-[10em] md:w-[25em] h-[25em] flex flex-col  text-[#4a3826]">
                  {/* <div className="rounded-md bg-[#e1dbd2c2]">
                    <h1
                      className={`flex justify-center p-5 text-xl md:text-2xl font-bold ${styles.slogan}`}
                    >
                      BACK TO THE ROOTS
                    </h1>
                    <p
                      className={`px-5 pb-5 text-md flex justify-center text-center ${styles.text}`}
                    >
                      All our collections are proudly designed and assembled in
                      Hong Kong.
                    </p>
                    <p
                      className={`px-5 pb-5 text-md hidden md:flex justify-center text-center ${styles.text}`}
                    >
                      And so, all are marked with <br /> &quot;BUILT IN HONG
                      KONG&quot;.
                    </p>
                  </div> */}
                </div>
                <Image
                  src={urlFor(products[0].images[index].asset.url)
                    .quality(75)
                    .url()} // ADJUST SIZE OR QUALITY TO FASTER LOADING
                  alt="chrono-parallax"
                  // width="w-full"
                  // height="500"
                  className={styles.bgWrapper}
                  objectFit="contain"
                  objectPosition="center"
                  layout="fill"
                ></Image>
              </SwiperSlide>
            );
          }))}
        </Swiper>
      </div>


      {/* RELATED PRODUCTS */}

      <div className="z-10 pb-16 pt-8 h-[30rem] flex flex-col items-center text-[#4a3826]">
        <h1 className={styles.slogan}>COLLECTIONS YOU MAY ALSO LIKE</h1>
        <div>
          <p>Insert mapping for products here.</p>
        </div>
      </div>
    </>
  );
}

export default Colour;



export const getServerSideProps: GetServerSideProps<Props> = async (params) => {
  // export const getStaticProps: GetStaticProps<Props> = async (paths) => {

  // const pid = params;
  // const colour = paths.params?.colour // GetStaticProps with GetStaticPaths
  // console.log("params.query in GSP: ", colour)
  // const { colour, collections }: any = params; // GetServerSideProps

  const colour = params.query.colour;
  const collections = params.query.collections;
  console.log(
    "params.query in GSSP: ",
    colour,
    ", params.collections in GSSP: ",
    collections
  );

  // const capitalisedColour = colour.charAt(0).toUpperCase() + colour.slice(

  // const products = await fetchProductsDynamic();
  const res = await fetch(
    // `${process.env.NEXT_PUBLIC_BASE_URL}/api/getProductsByCategory/${colour}` // TESTING
    `${process.env.NEXT_PUBLIC_BASE_URL}/api/getCategories/${collections}/${colour}`
  );

  // Caching
  // res.setHeader(
  //   'Cache-Control',
  //   'public, s-maxage=10, stale-while-revalidate=59'
  // )

  // const res = await dynamicProductHandler()

  console.log("pages/[collections]/[colour].tsx res: ", '\n', res);
  const data = await res.json();
  console.log("pages/[collections]/[colour].tsx data: ", '\n', data);
  const products: Product[] = data.products;

  return {
    props: {
      // categories,
      products,
      // session,
    },
  };
};

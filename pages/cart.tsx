import React from "react";
import CartContainer from "../src/components/CartContainer";

export default function Page() {
  return (

    <div
      id="cart"
      className="h-full pt-[6em] md:(h-full md:pt-[6em] md:pb-[3em] bg-[#e1dbd2]"
    >
      CART PAGE *** RMB TO USE FORM TAG ***
      <CartContainer />
    </div>
  );
};

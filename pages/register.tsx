import React, { FC } from "react";
import SignUpForm from "../src/components/SignUpForm";
import { signOut, useSession } from "next-auth/react";
import Link from "next/link";

const Register: FC = ({ }) => {

  const { data, status } = useSession();

  if (status === 'loading') return <p>Loading...</p>;

  return (
    <>
      {data ?
        <>
          <div className=" h-screen flex justify-center items-center ">
            <div>
              <h1 className='font-thin flex justify-center pb-6'> <p>Welcome back, {data?.user?.name} </p></h1>
              <Link href={'/my-account'} passHref className='font-thin flex justify-center pb-6'>
                <p>Go to My Account Page</p>
              </Link>


              <div className="flex text-lg text-nowrap font-thin justify-center flex-no-wrap">
                <p>Not your account?  &nbsp;
                </p>
                <Link
                  className="cursor-pointer  "
                  id="accountMenuItems"
                  href={'/'}
                  passHref
                  onClick={() => signOut({ redirect: false })}
                >
                  <p>Click here to LOG OUT.</p>
                </Link>

              </div>
            </div>
          </div>
        </>

        :
        <div className=" h-screen flex justify-center items-center ">
          <SignUpForm />
        </div >}

    </>
  );
};

export default Register;

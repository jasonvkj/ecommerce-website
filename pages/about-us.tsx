import React from "react";

const About = () => {
  return (
    <>
      <div
        id="about-us"
        className="h-[50rem] md:h-[60rem] flex justify-center items-center"
      >
        <div>
          ABOUT US
        </div>
      </div>
    </>
  );
};

export default About;

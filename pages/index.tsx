import type { GetServerSideProps, NextPage } from "next";

import Head from "next/head";

import Header from "../src/components/Header";
import Landing from "../src/components/Landing";
import { fetchCategories } from "../src/utils/fetchCategories";
import { fetchProducts } from "../src/utils/fetchProducts";
import getSession from "./api/getSession";
import { Tab } from "@headlessui/react";
import { useSession } from "next-auth/react";
import { getServerSession } from "next-auth";
import { authOptions } from "./api/auth/[...nextauth]";
// import { Props } from "next/script";
// import "../styles/globals.css";

export const metadata = {
  title: {
    default: 'TypeScript Next.js Stripe Example',
    template: '%s | Next.js + TypeScript Example',
  }
}

// interface Props {
//   categories: Category[];
//   products: Product[];
//   // session: Session | null;
//   // session: null;
// }

// const Home = ({ categories, products }: Props) => {
//console.log(products);
// const session = await getServerSession(authOptions);

// const showProducts = (category: number) => {
//   return products
//     .filter((product) => product.category._ref === categories[category]._id)
//     .map((product) => <Product product={product} key={product._id} />); // filter products by category
// };
const Home = () => {
  return (
    <>

        <Head>
          <title>Ecommerce with NextJS</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {/* <Header /> */}
        <main>
          <Landing />
        </main>


    </>
  );
};

export default Home

// Backend Code

// export const getServerSideProps = async (
//   //context: any
// ) => {
//   const categories = await fetchCategories();
//   const products = await fetchProducts();
//   //const session = await getServerSession(authOptions)

//   return {
//     props: {
//       categories,
//       products,
//       //session,
//     },
//   };
// };

// export const getServerSideProps: GetServerSideProps<Props> = async (
//   // context
// ) => {
//   const categories = await fetchCategories();
//   const products = await fetchProducts();
//   // const session = await getSession(context);

//   return {
//     props: {
//       categories,
//       products,
//       // session,
//     },
//   };
// };

/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  distDir: "build",
  images: {
    domains: ["cdn.sanity.io"],
  },
  i18n: {
    // Hide locale for developement
    localeDetection: false,

    // These are all the locales you want to support in
    // your application
    locales: [ "en-US", "en-GB", "zh-HK",],

    // This is the default locale you want to be used when visiting
    // a non-locale prefixed path e.g. `/hello`
    defaultLocale: "en-GB",
    // This is a list of locale domains and the default locale they
    // should handle (these are only required when setting up domain routing)
    // Note: subdomains must be included in the domain value to be matched e.g. "fr.example.com".
    // domains: [
    //   {
    //     domain: "example.com", // Change to "burlapwatch.com"
    //     defaultLocale: "en-GB",
    //   },
    //   {
    //     domain: "example.nl", // Change to "burlapwatch.com"
    //     defaultLocale: "nl-NL",
    //   },
    //   {
    //     domain: "example.fr", // Change to "burlapwatch.com"
    //     defaultLocale: "fr",
    //     // an optional http field can also be used to test
    //     // locale domains locally with http instead of https
    //     http: true,
    //   },
    // ],
  },
  
};

module.exports = nextConfig;
// module.exports = {
//   images: {
//     // domains:['assets.example.com'],
//     loader: 'imgix',
//     path: '/',

//   },
// }

/** @type {import('tailwindcss').Config} */

// const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [
    // "./node_modules/flowbite-react/**/*.js",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
    "./public/**/*.html",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "bg_landing": "url(('./img/bg-landing.jpg))",
      },
      fontFamily: {
        'product':['"Passion One", ']

      }
    },
  },
  plugins: [
    // require("flowbite/plugin")
    require('@headlessui/tailwindcss'),
  ],
};

interface Category {
  _id: string;
  _createdAt: string;
  _updatedAt: string;
  _rev: string;
  _type: "category";
  slug: {
    _type: "slug";
    current: string;
  };
  title: string;
  images: Image[];
  price: number;
  sku: string;
}

interface Image {
  _key: string;
  _type: "image";
  asset: {
    url: string;
  };
  crop: {
    _type: string;
    bottom: number;
    left: number;
    right: number;
    top: number;
  };
}

interface Body {
  _key: string;
  _type: string;
  description: string;
}

interface Product {
  _id: string;
  _createdAt: string;
  _updatedAt: string;
  _rev: string;
  _type: "product";
  title: string;
  colour: string;
  slug: {
    _type: "slug";
    current: string;
  };
  description: string;
  category: {
    _type: "reference";
    _ref: string;
  };
  // categoryName: string;
  price: number;
  sku: string;
  image: Image[];
  images: Image[];
  gallery: Image[];
  body: Body[];
  collection: string;
  functions: string;
  caseMaterial: string;
  caseDiameter: string;
  caseHeight: string;
  caseWaterRest: string;
  caseFeats: string;
  dialFinish: string;
  dialLume: string;
  dialIndex: string;
  calibreName: string;
  calibreModel: string;
  calibreBatteryLife: string;
  calibreAccuracy: string;
  calibreJewels: string;
  lensFinish: string;
  lensMaterial: string;
  strapName: string;
  strapStyle: string;
}

interface StripeProduct {
  id: string;
  amount_discount: number;
  amount_subtotal: number;
  amount_tax: number;
  amount_total: number;
  currency: string;
  description: string;
  object: string;
  quantity: number;
  price: {
    unit_amount: number;
  };
}

interface Account {
  id: string;
  username: string;
  password: string;
  email: string;
  images: Image[];
}

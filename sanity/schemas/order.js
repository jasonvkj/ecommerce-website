export default {
    name: "order",
    title: "Order",
    type: "document",
    fields: [
      {
            name: "invoice",
            title: "Invoice",
            type: "string",
      },
      {
        name: "title",
        title: "Title",
        type: "string",
      },
      {
        name: "colour",
        title: "Colour",
        type: "string",
      },
      {
        name: "slug",
        title: "Slug",
        type: "slug",
        options: {
          source: "title",
          maxLength: 96,
        },
      },
      {
        title: "Quantity",
        name: "pcs",
        type: "number",
      },
      {
        title: "Price",
        name: "price",
        type: "number",
      },
      {
        title: "SKU",
        name: "sku",
        type: "string",
      },
      {
        name: "images",
        title: "Images",
        type: "array",
        of: [
          {
            type: "image",
            options: {
              hotspot: true,
              crops: true,
            },
          },
        ],
      },
      {
        name: "user",
        title: "User",
        type: "reference",
        to: [{ type: "user" }],
      },
      {
        name: "category",
        title: "Category",
        type: "reference",
        to: [{ type: "category" }],
      },
    //   {
    //     name: "body",
    //     title: "Body",
    //     type: "localeBlockContent",
    //   },
  
    //   {
    //     name: "gallery",
    //     title: "Gallery",
    //     type: "array",
    //     of: [
    //       {
    //         type: "image",
    //         options: {
    //           hotspot: true,
    //         },
    //       },
    //     ],
    //   },
    ],
    
  
    preview: {
      select: {
        title: "title",
        media: "images.0.asset",
      },
    },
  };
  
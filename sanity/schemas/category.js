export default {
  name: "category",
  title: "Category",
  type: "document",
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      title: "Price",
      name: "price",
      type: "number",
    },
    {
      title: "SKU",
      name: "sku",
      type: "string",
    },
    {
      name: "images",
      title: "Images",
      type: "array",
      of: [
        {
          type: "image",
          options: {
            hotspot: true,
          },
        },
      ],
    },
    {
      name: "description",
      title: "Description",
      type: "text",
    },
    {
      name: "functions",
      title: "Functions",
      type: "text",
    },

    {
      name: "caseMaterial",
      title: "Case Material",
      type: "string",
    },
    {
      name: "caseDiameter",
      title: "Case Diameter",
      type: "string",
    },
    {
      name: "caseHeight",
      title: "Case Height",
      type: "string",
    },
    {
      name: "caseWaterRest",
      title: "Case Water Resistance",
      type: "string",
    },
    {
      name: "caseFeats",
      title: "Case Features",
      type: "text",
    },
 
    {
      name: "dialFinish",
      title: "Dial Finish",
      type: "string",
    },
    {
      name: "dialLume",
      title: "Dial Lume",
      type: "string",
    },
    {
      name: "dialIndex",
      title: "Dial Index",
      type: "string",
    },
  
    {
      name: "calibreName",
      title: "Calibre Name",
      type: "string",
    },
    {
      name: "calibreModel",
      title: "Calibre Model",
      type: "string",
    },
    {
      name: "calibreBatteryLife",
      title: "Calibre Battery Life",
      type: "string",
    },
    {
      name: "calibreAccuracy",
      title: "Calibre Accuracy",
      type: "string",
    },
    {
      name: "calibreJewels",
      title: "Calibre Jewels",
      type: "string",
    },

    {
      name: "lensFinish",
      title: "Lens Finish",
      type: "string",
    },
    {
      name: "lensMaterial",
      title: "Lens Material",
      type: "string",
    },

    {
      name: "strapName",
      title: "Strap Name",
      type: "string",
    },
    {
      name: "strapStyle",
      title: "Strap style",
      type: "string",
    },
  ],

  preview: {
    select: {
      title: "title",
      media: "images.0.asset",
    },
  },
};

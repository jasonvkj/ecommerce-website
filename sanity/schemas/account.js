export default {
  name: 'account',
  title: 'Account',
  type: 'document',
  fields: [
    {
      name: 'providerType',
      title: 'ProviderType',
      type: 'string'
    },
    {
      name: 'providerId',
      title: 'ProviderId',
      type: 'string'
    },
    {
      name: 'providerAccountId',
      title: 'ProviderAccountId',
      type: 'string'
    },
    {
      name: 'refreshToken',
      title: 'RefreshToken',
      type: 'string'
    },
    {
      name: 'accessToken',
      title: 'AccessToken',
      type: 'string'
    },
    {
      name: 'accessTokenExpires',
      title: 'AccessTokenExpires',
      type: 'number'
    },
    {
      name: 'user',
      title: 'User',
      type: 'reference',
      to: { type: 'user' }
    }
  ]
};
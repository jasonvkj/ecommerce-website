export default {
  name: "product",
  title: "Product",
  type: "document",
  fields: [
    {
      name: "title",
      title: "Title",
      type: "string",
    },
    {
      name: "colour",
      title: "Colour",
      type: "string",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      title: "Weight in grams",
      name: "grams",
      type: "number",
    },
    {
      title: "Price",
      name: "price",
      type: "number",
    },
    {
      title: "SKU",
      name: "sku",
      type: "string",
    },
    {
      name: "images",
      title: "Images",
      type: "array",
      of: [
        {
          type: "image",
          options: {
            hotspot: true,
            crops: true,
          },
        },
      ],
    },
    {
      title: "Bar code",
      name: "barcode",
      type: "barcode",
    },
    {
      title: "Tags",
      name: "tags",
      type: "array",
      of: [
        {
          type: "string",
        },
      ],
      options: {
        layout: "tags",
      },
    },
    {
      name: "blurb",
      title: "Blurb",
      type: "localeString",
    },
    // {
    //   name: "categories",
    //   title: "Categories",
    //   type: "array",
    //   of: [
    //     {
    //       type: "reference",
    //       to: { type: "category" },
    //     },
    //   ],
    // },
    {
      name: "category",
      title: "Category",
      type: "reference",
      to: [{ type: "category" }],
    },
    {
      name: "body",
      title: "Body",
      type: "localeBlockContent",
    },

    {
      name: "gallery",
      title: "Gallery",
      type: "array",
      of: [
        {
          type: "image",
          options: {
            hotspot: true,
          },
        },
      ],
    },
  ],
  

  preview: {
    select: {
      title: "title",
      media: "images.0.asset",
    },
  },
};

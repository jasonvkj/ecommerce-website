export const fetchProductsDynamic = async () => {
  const res = await fetch(
`${process.env.NEXT_PUBLIC_BASE_URL}/api/getProductsDynamic` // TESTING
  );

  const data = await res.json();
  const products: Product[] = data.products;

  return products;
};

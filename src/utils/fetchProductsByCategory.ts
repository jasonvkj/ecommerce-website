export const fetchProductsByCategory = async () => {
  
  const res = await fetch(
`${process.env.NEXT_PUBLIC_BASE_URL}/api/getProductsByCategory/` // TESTING
  );

  const data = await res.json();
  const categories: Category[] = data.categories;

  return categories;
};

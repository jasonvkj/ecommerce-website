import {
    blueBox,
    myAccountImage,
    myOrdersImage,
    preferencesImage,
    savedItemsImage,
    signOutImage,
    theClassic,
  } from "../components/Images";
  import { MenuItems, AccountTabItems, CollectionList } from "../types";
  
  export const menuItems: MenuItems[] = [
    {
      name: "HOME",
      path: "/",
    },
    {
      name: "ABOUT",
      path: "/about-us",
    },
    {
      name: "COLLECTIONS",
      path: "/collections",
    },
    ,
    {
      name: "SHOPPING CART",
      path: "/my-cart",
    }
  ];
  
  export const accountTabItems: AccountTabItems[] = [
    {
      name: "My Profile",
      path: "/my-account",
      image: myAccountImage(),
    },
    // {
    //   name: "Edit Profile",
    //   path: "/my-account/edit",
    //   image: "b",
    // },
    // {
    //   name: "Preferences",
    //   path: "/preferences",
    //   image: preferencesImage(),
    // },
    {
      name: "My Orders",
      path: "/my-orders",
      image: myOrdersImage(),
    },
    {
      name: "Favourites",
      path: "/saved-items",
      image: savedItemsImage(),
    },
    // {
    //   name: "Sign Out",
    //   path: "/log-out",
    //   image: signOutImage(),
    // },
  ];
  
  export const collectionItems: CollectionList[] = [
    {
      model: "The Classic",
      path: "/the-classic",
      image: theClassic(),
      price: "$1,000",
    },
    {
      model: "Chronograph First",
      path: "/chronograph-first",
      image: theClassic(),
      price: "$1,000",
    },
    {
      model: "The Heritage",
      path: "/the-heritage",
      image: theClassic(),
      price: "$1,000",
    },
    {
      model: "Tourbillon Propeller",
      path: "/tourbillon-propeller",
      image: theClassic(),
      price: "$1,000",
    },
    {
      model: "The Harbour",
      path: "/the-harbour",
      image: theClassic(),
      price: "$1,000",
    },
    {
      model: "Power Reserve",
      path: "/power-reserve",
      image: theClassic(),
      price: "$1,000",
    },
    {
      model: "Lion Rock",
      path: "/lion-rock",
      image: theClassic(),
      price: "$1,000",
    },
    {
      model: "Moon Phase",
      path: "/moon-phase",
      image: theClassic(),
      price: "$1,000",
    },
  ];
  
import React from "react";

export default function Footer() {
  return (
    <footer className="h-[10rem] mt-2 mb-2 pt-2 flex flex-col justify-center items-center text-gray-400">
      <p>Designed, Built by JW. Powered by AWS</p>
      <p>© All Rights Reserved 2024</p>
    </footer>
  );
}

import { useRouter } from "next/router";
import useSwr from "swr";

type Product = {
  name: string;
};

const fetcher = (url: string) => fetch(url).then((res) => res.json());

export default function productPage() {
  const router = useRouter();
  const { data, error } = useSwr<Product>(
    router.query.id ? `/api/user/${router.query.id}` : null,
    fetcher
  );
  return <div>[product]</div>;
}

import { useState } from 'react';
import { signIn, signOut, useSession } from 'next-auth/react';
import { signUp } from 'next-auth-sanity/client';
import { useRouter } from 'next/navigation';
import styles from "../../src/styles/SignUpForm.module.css"
import Link from 'next/link';
import { myAccountImage, signOutImage } from './Images';


export default function SignUpForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');
  const [signUpButton, setSignUpButton] = useState(false);

  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    await signUp({
      email,
      password,
      name
    });

    await signIn('sanity-login', {
      redirect: false,
      email,
      password
    });

    router.refresh();
  };

  const handleSubmitSignIn = async (e: React.FormEvent) => {
    e.preventDefault();

    await signIn('sanity-login', {
      redirect: false,
      email,
      password
    })
      .then((result) => {

        if (result?.error)
          alert("Invalid Credentials!")
        else
          window.location.replace("/my-account")
      })
      .catch(err => {
        alert(`Error Occured: ${err}`)
      })

    //router.refresh();
  };

  //   useEffect(() => {
  //     if (Cookies.get('token')) {
  //         Router.push('/');
  //     }
  // }, [Router])

  const { data, status } = useSession();

  return (
    <div className='signUpForm bg-gray-400 w-full flex flex-col  px-2 py-6 '>
      {/* <h1 className='font-thin flex justify-center'>Testing case: {(data) ? <p>Welcome back, {data?.user?.name} </p> : "There is no user logged in now."}</h1> */}

        <div className='flex justify-center mt-4'>
          {!signUpButton ?
            <>
              <form onSubmit={handleSubmitSignIn} className='w-full'>
                {/* <h3>Sign In</h3> */}

                <div><h1>Email</h1>
                </div>

                <div><input
                  type="email"
                  value={email}
                  placeholder="Email"
                  onChange={e => setEmail(e.target.value)}
                  className="p-2 w-full" />
                </div>


                <div><h1>Password</h1></div>
                <div><input
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={e => setPassword(e.target.value)}
                  className="p-2 w-full" />
                </div>
                <div className='flex justify-center'>
                  <button type="submit" className={` rounded-md px-4 mt-4 mb-4 ${styles.button}`}>Sign In</button>
                </div>

                <h1>Need an account? &nbsp;
                  <button type='button' className={styles.signUpButton} onClick={() => setSignUpButton(!signUpButton)}>
                    Register here
                  </button>
                </h1>

              </form>
            </>
            :
            ""}

        </div>

      


      {
        signUpButton ? <div className='flex justify-center'>
          <form onSubmit={handleSubmit} className='w-full' >
            {/* <h3>Register </h3> */}

            <div><h1>Email</h1></div>
            <div>
              <input
                type="email"
                placeholder="Email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                className="p-2 w-full "
              />

            </div>

            <div><h1>Password</h1></div>
            <div>
              <input
                type="password"
                placeholder="Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                className="p-2 w-full "
              />
            </div>

            <div><h3>Name</h3></div>
            <div>
              <input
                type="name"
                placeholder="Name"
                value={name}
                onChange={e => setName(e.target.value)}
                className="p-2 w-full "
              />
            </div>

            <div className='flex justify-center'>
              <button type="submit" className={` rounded-md px-4 mt-4 ${styles.button}`}>Create Account</button>
            </div>

            <h1>Already have an account? <button type='button' className={styles.signUpButton} onClick={() => setSignUpButton(!signUpButton)}>Log in here</button> </h1>

          </form>
        </div> : ""
      }


    </div >

  );
}
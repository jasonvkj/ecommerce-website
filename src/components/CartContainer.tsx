import { Transition } from "@headlessui/react";
import { ShoppingBagIcon } from "@heroicons/react/20/solid";
import React, { useState } from "react";
import { useCart } from "../CartContext";
import { CartItemView } from "./CartItem";

export default function CartContainer() {

  const [isShowing, setIsShowing] = useState(false);

  const { cartItems, cartTotal } = useCart();

  return (
    <>

      <h1 className="flex justify-center h-20 ">

        MY SHOPPING CART
      </h1>
      <form className="flex flex-col lg:pl-10 lg:pr-10  ">
        <div className="flex flex-col justify-center ml-10 mr-10 ">


          {cartItems.length === 0 ? (
            <h1
              style={{
                textAlign: "center",
              }}
            >
              Your cart is empty
            </h1>
          ) : (
            <>
              {/* <table className="table-auto">
                <thead>
                  <tr>
                    <th>
                      ITEM
                    </th>
                    <th>
                      PRICE
                    </th>
                    <th>
                      QUANTITY
                    </th>
                    <th>SUB-TOTAL</th>
                  </tr>
                </thead>

              </table> */}

              {/* Listing all items in the cart */}
              {cartItems.map((item: any) => (
                <div key={item.products[0]._id} className="">
                  <CartItemView item={item} />
                </div>
              ))}

              <div className="flex flex-col ">
                <div className="mt-2 flex justify-end">
                  <h1 >
                    Total Cost: ${cartTotal.toFixed(2)}
                  </h1>
                </div>

                <div className="mt-2 mb-6" >
                  <button className="outline outline-offset-2 outline-2 outline-black">Checkout</button>
                </div>

              </div>

            </>
          )}
        </div>






      </form>

    </>
  );
}

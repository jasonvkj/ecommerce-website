import { CartItem } from "../types";
import { useCart } from "../CartContext";
import { Grid } from "swiper";
import { minusButton, plusButton, removeCartButton } from "./Images";
import Image from "next/legacy/image";
import { urlFor } from "../../sanity";

import styles from "../../src/styles/Products.module.css";



interface Props {
    item: CartItem;
}

export const CartItemView = ({ item }: Props) => {
    const { removeFromCart, updateCartItemQuantity } = useCart();

    const handleQuantityChange = (qty: number) => {
        const quantity = Number(qty);
        if (quantity >= 1) {
            updateCartItemQuantity(item.products[0]._id, quantity);
        }
    };

    const handleRemoveClick = () => {
        removeFromCart(item.products[0]._id);
    };
    console.log(item)
    return (

        <div className="flex justify-start py-5 w-16">


            {/* Product Image and Title */}
            <div className="">
                <Image
                    src={urlFor(item.products[0].images[0].asset.url).url()}
                    alt={item.products[0].title}
                    width={150}
                    height={150}
                    objectFit="contain"
                />
            </div>

            {/* Product Desc & Quantity */}
            <div className="flex flex-col items-center">
                <div className="">
                    <p className="text-lg flex ">
                        {item.products[0].title}
                    </p>

                </div>


                <div className="">
                    <p className="text-[1.25em] flex justify-center">
                        ${(item.products[0].price * item.quantity).toFixed(2)}
                    </p>


                </div>

                {/* Increasing/Decreasing Product Quantity Button  */}

                <div>
                    <div className="flex justify-center">
                        <button
                            aria-label="decrease"
                            onClick={(e) => {
                                e.preventDefault();
                                handleQuantityChange(item.quantity - 1);
                            }}
                        >
                            {minusButton()}
                        </button>
                        <p>
                            &nbsp;{item.quantity}&nbsp;
                        </p>
                        <button
                            aria-label="increase"
                            onClick={(e) => {
                                e.preventDefault();
                                handleQuantityChange(item.quantity + 1);
                            }}
                        >
                            {plusButton()}
                        </button>
                    </div>
                </div>

                {/* Removing Product From Cart */}
                <div className="flex justify-center">
                    <button
                        aria-label="remove"
                        onClick={handleRemoveClick}
                    >

                        {removeCartButton()}
                    </button>
                </div>
            </div>

        </div>
    )
}
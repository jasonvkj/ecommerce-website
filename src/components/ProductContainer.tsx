import { count } from "console";
import { GetServerSideProps } from "next";
// import { Props } from "next/script";
import Image from "next/legacy/image";

import React from "react";
import { urlFor } from "../../sanity";
import { fetchProductsDynamic } from "../utils/fetchProductsDynamic";
import styles from "../../src/styles/Products.module.css";

interface Props {
  products: Product[];
}

export const ProductContainer = ({ products }: Props) => {
  const [count, setCount] = React.useState(1);
  const decrementCounter = () => {
    // setCount(prevCount => ({value: Math.max(prevCount.value - 1, 0)}));
    setCount((prevCount) => Math.max(prevCount - 1, 0));
  };

  const incrementCounter = () => {
    setCount((prevCount) => prevCount + 1);
  };

  return (
    <>
      <div
        id="collections"
        className="h-full pt-[6em] md:h-full md:pt-[6em] md:pb-[3em] flex justify-center md:justify-around items-center bg-[#e1dbd2]"
      >
        <div className="grid grid-flow-cols-2 md:grid-cols-2">
          <div className="grid justify-items-center content-center ">
            {/* <Swiper
              modules={[Zoom, Navigation, Pagination, A11y]}
              spaceBetween={50}
              slidesPerView={1}
              // navigation
              pagination={{ clickable: true }}
              zoom={true}
              onSlideChange={() => console.log("slide change")}
              onSwiper={(swiper) => console.log(swiper)}
              className="grid "
            >
              <SwiperSlide> */}
            <Image
              src={urlFor(products[0].images[0].asset.url).url()}
              alt="watch-template"
              width="500"
              height="500"
              className={styles.watchTemplate}
              objectFit="contain"
            ></Image>
            {/* </SwiperSlide>
            </Swiper> */}
          </div>
          <div className="mt-[2em] md:m-0 flex flex-col justify-center bg-white md:bg-[#e1dbd2] ">
            {/* <p className={`${styles.itemTitle}`}>Chronograph First</p> */}
            {/* <p className={`${styles.itemTitle}`}>{products[0].category}</p> */}
            <p className={`${styles.itemTitle}`}>
              {products[0].title.toUpperCase()}
            </p>
            {/* <p className={`${styles.itemSubTitle}`}>{products[0].colour}</p> */}
            <p className={`${styles.price}`}>
              HK${products[0].price.toLocaleString()}
            </p>
            <p className={`${styles.itemDesc}`}>
              Designed for any situations, the Burlap completes your outfit with
              a tasteful vintage touch.
            </p>
            <p className={`${styles.itemDesc}`}>
              May this statement piece inspire perseverance in what you love.
            </p>

            <div className="flex flex-col justify-between items-center">
              <div
                className={`flex justify-around items-center ${styles.counterContainer}`}
              >
                <div className=" ">
                  <button
                    className="flex justify-center items-center h-full w-[10em]"
                    onClick={decrementCounter}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M19.5 12h-15"
                      />
                    </svg>
                  </button>
                </div>
                <div className={styles.counter}>{count}</div>
                <div className="">
                  <button
                    className="flex justify-center items-center h-full w-[10em]"
                    onClick={incrementCounter}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="currentColor"
                      className="w-6 h-6"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M12 4.5v15m7.5-7.5h-15"
                      />
                    </svg>
                  </button>
                </div>
              </div>

              <div className={styles.orderButton}>
                <button className="flex justify-center items-center h-full w-full">
                  ADD TO CART
                </button>
              </div>
              {/* <div>
                <button onClick={() => setCount(1)}>RESET</button>
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}




export const getServerSideProps: GetServerSideProps<Props> = async (
  context
) => {
  const products = await fetchProductsDynamic();
  return {
    props: {
      // categories,
      products,
      // session,
    },
  };
};

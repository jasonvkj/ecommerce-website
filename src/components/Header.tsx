import { Transition } from "@headlessui/react";
import { useSession, signOut } from "next-auth/react";
import Link from "next/link";
import { FC, useState } from "react";
import { menuItems } from "../utils/menuList";
import { signOutImage, signInImage } from "./Images";
import Image from "next/legacy/image";



import homeLogo from "../../src/static/media/Burlap_logo_black.png";
import styles from "../../src/styles/Header.module.css";
import { ShoppingCartIcon } from "@heroicons/react/20/solid";
import router from "next/router";


const Header: FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [showLogin, setShowLogin] = useState(true);
  const [isClicked, setIsClicked] = useState(Boolean);

  // const username = useSelector((state: RootState) => state.user.user.username)
  // const role = useSelector((state: RootState) => state.user.user.role)

  function switchForm() {
    showLogin === true ? setShowLogin(false) : setShowLogin(true);
  }

  // useEffect(() => {
  //   if (role === 'user' || role === 'editor') {
  //     setShowModal(false)
  //   }
  // }, [role])

  const { data, status } = useSession();



  return (
    // Add Fixed if want to set navbar fixed to top
    <nav className={`fixed w-full z-2  bg-white ${styles.header}`}>

      <div className="w-full flex flex-row justify-between items-center px-2 md:px-5 pb-4">
        {/* Mobile Menu Button */}

        <div className="basis-1/4">
          <button
            onClick={() => setIsOpen(!isOpen)}
            type="button"
            className=" bg-inherit inline-flex items-center justify-center ml-4 mr-4 p-2 text-[#4a3826]  hover:bg-inherit focus:border-none focus:ring-2 after:ring-offset-2 focus:ring-offset-blue-800 focus:ring-white active:outline-none"
            aria-controls="mobile-menu"
            aria-expanded="false"
          >
            {!isOpen ? <div>
              <p className="menuButton text-xl">MENU</p>
            </div>
              :
              <div>
                <p className="menuButton text-xl">CLOSE</p>
              </div>
            }

          </button>
        </div>

        {/* HEADER LOGO */}
        <div className="flex basis-1/2 items-center justify-center md:justify-between">
          <Link
            href="/"
            // legacyBehavior
            className="flex justify-center items-center  "
          >
            <Image
              src={homeLogo}
              alt="Home Logo"
              width={100}
              height={20}

              className="object-fit"

            />
          </Link>
        </div>
        <div className="basis-1/4">
          <Link
            href="/cart"
            // legacyBehavior
            className="flex justify-center items-center p-2"
          >
            <p className="menuButton text-xl">CART</p>
            {/* <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor"
              className="block h-7 w-7">
              <path strokeLinecap="round" strokeLinejoin="round" d="M2.25 3h1.386c.51 0 .955.343 1.087.835l.383 1.437M7.5 14.25a3 3 0 0 0-3 3h15.75m-12.75-3h11.218c1.121-2.3 2.1-4.684 2.924-7.138a60.114 60.114 0 0 0-16.536-1.84M7.5 14.25 5.106 5.272M6 20.25a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Zm12.75 0a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Z" />
            </svg> */}


          </Link>


        </div>
      </div>



      {/* MOBILE RESPONSIVE MENU */}
      <Transition
        show={isOpen}
        enter="transition ease-out duration-100 transform"
        enterFrom="opacity-0 scale-95"
        enterTo="opacity-100 scale-100"
        leave="transition ease-in duration-75 transform"
        leaveFrom="opacity-100 scale-100"
        leaveTo="opacity-0 scale-95"
      >
        {/* {(ref) => ( */}

        <div className="bg-transparent px-5 mt-2">
          <ul className=" px-2 pt-2 space-y-1 sm:px-3">
            {menuItems.map((menuItem, index) => {
              return (
                <li
                  key={index}
                  className=" justify-items-center marker:none "
                  id="mobileMenuItems"
                >
                  <Link
                    className={` cursor-pointerml-5 ${styles.mobileMenuItems}`}
                    href={menuItem.path}
                    passHref
                  >
                    {menuItem.name}
                  </Link>
                </li>
              );
            })}
            <hr className="separator solid"></hr>

            {(!data) ?
              <>
                <Link
                  className="cursor-pointer flex  pt-2 pb-2 items-center "
                  id="accountMenuItems"
                  href='/register'
                  passHref

                >
                  <p className={`ml-5 ${styles.mobileMenuItems}`}>LOG IN</p>
                </Link>
              </> :

              <>
                <p className="text-nowrap pb-4">Welcome back, {(data) ? data?.user?.name : "Guest"}</p>
                <Link className="cursor-pointer" href="/my-account" passHref>
                  <p className={`ml-5 ${styles.mobileMenuItems}`}>MY ACCOUNT</p>
                </Link>
                <Link
                  className="cursor-pointer flex pb-6 justify-between items-center"
                  id="accountMenuItems"
                  href={'/'}
                  passHref
                  onClick={() => signOut({ redirect: false })}
                >
                  {/* <div>{signOutImage()}</div> */}

                  <p className={`ml-5 ${styles.mobileMenuItems}`}>
                    LOG OUT
                  </p>
                </Link>
              </>}



          </ul>
        </div>
        {/* )} */}
      </Transition>


    </nav >
  );
}

export default Header;

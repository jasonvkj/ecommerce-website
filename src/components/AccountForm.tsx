import { useSession } from 'next-auth/react';
import { useRouter } from 'next/router';
import React, { useState } from 'react'

import Image from "next/legacy/image";
import profilePic from "../../public/blank-profile-picture.png";
import Link from 'next/link';
import { signInImage } from './Images';


export const AccountForm = () => {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');

    const { data, status } = useSession();
    console.log(data);

    const router = useRouter();

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();


        router.refresh();
    };




    return (
        <><div
            id="my-account"
            className="h-[50rem] md:h-[60rem] flex flex-col justify-center items-center"
        >
            {(data) ?
                <form onSubmit={handleSubmit}><div>

                    <p>Welcome back, {(data) ? data?.user?.name : "Guest"}</p>

                    <Image
                        src={profilePic}
                        width="100em"
                        height="100em"
                        alt="profile-picture"
                        className="object-cover"
                    />
                </div>
                    <div>
                        <h2>Username</h2>
                        <h2 className="font-bold">{(data) ? data?.user?.name : "Guest"}</h2>

                    </div>
                    <div>

                    </div>
                </form>

                : <Link
                    className="cursor-pointer flex pb-6 justify-between items-center"
                    id="accountMenuItems"
                    href='/register'
                    passHref

                >
                    CLICK HERE TO REGISTER OR LOG IN.

                </Link>}

        </div>
        </>
    )
}

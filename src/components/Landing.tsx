import React from "react";
import Image from "next/legacy/image";
import Link from "next/link";

import styles from "../../src/styles/Landing.module.css";
import landingBanner from "../../public/bg-landing.jpg";
import burlapAlDiver from "../../public/BP00905_05.jpeg";
import "@fontsource/work-sans/700.css";
import { ChevronRightIcon } from "@heroicons/react/20/solid";


function Landing() {
  // const [openModal, setOpenModal] = useState();
  return (
    <>
      <div
        id="landing-section-1"
        className=" h-screen  flex justify-center items-center"
      >
        <div className="z-8 pb-[16em] md:pb-17 pl-6 md:pl-0 flex flex-col justify-center md:items-center text-[#4a3826]">
          <h1 className={`text-xl md:text-2xl font-bold ${styles.slogan}`}>
            TIMEKEEPING OUR HERITAGE
          </h1>
          <p className="text-sm  md:text-md ">by BURLAP WATCHES </p>
        </div>
        {/* <Image
          src={landingBanner}
          alt="landing-photo"
          // max-width="100rem"
          // max-height="100rem"
          className={styles.bgWrapper}
          layout="fill"
          priority
        /> */}
      </div>
      <div
        id="landing-section-2"
        className="relative h-screen  flex justify-center items-center"
      >
        <div className="z-8 pl-6 pr-6 pb-[30em] md:pb-[50em] md:pb-17 md:pl-0 flex flex-col justify-center md:items-center text-[#4a3826]">
          <h1 className={`space-y-0 ${styles.title}`}>OUR TIMELESS HERITAGE</h1>
          <p className={`text-md ${styles.text}`}>
            On a timekeeping mission, we believe our core value as a heritage to
            tell the world what Hong Kong stands for.
          </p>
          <Link
            className="cursor-pointer pt-10 text-sm flex items-center"
            href="/collections"
            passHref
          >
            VIEW OUR COLLECTIONS <ChevronRightIcon className="h-[1em] w-[1em" />
          </Link>
        </div>
        {/* <Image
          src={burlapAlDiver} // Amazon S3 Bucket Link - dynamic please
          alt="landing-photo-watch"
          // height="300rem"
          // width="300rem"
          quality="100"
          layout="fill"
          className={styles.bgWrapper}
        /> */}
      </div>
      <div
        id="landing-section-3"
        className="relative h-screen  flex justify-center items-center"
      >
        <div className="z-10 pb-16 pt-8 h-[30rem] flex flex-col items-center text-[#4a3826]">
          <h1 className={styles.subTitle}>EXPLORE OUR COLLECTIONS</h1>
          <div>
            <p>Insert mapping for products here.</p>
          </div>
        </div>
      </div>
    </>
  );
}

export default Landing;

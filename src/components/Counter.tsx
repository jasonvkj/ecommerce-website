'use client'
import React, { useState } from "react";
import styles from "../styles/Counter.module.css";

export default function Counter() {
  const [count, setCount] = React.useState(1);

  const decrementCounter = () => {
    // setCount(prevCount => ({value: Math.max(prevCount.value - 1, 0)}));
    setCount((prevCount) => Math.max(prevCount - 1, 0));
  };

  const incrementCounter = () => {
    setCount((prevCount) => prevCount + 1);
  };

  if (count < 0) {
  }
  return (
    <>
      <div className="flex justify-center">
        <div>
          <button onClick={decrementCounter}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M19.5 12h-15"
              />
            </svg>
          </button>
        </div>

        <div className={styles.counter}>{count}</div>

        <div>
          <button onClick={incrementCounter}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-6 h-6"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M12 4.5v15m7.5-7.5h-15"
              />
            </svg>
          </button>
        </div>
      </div>

      <div>
        <button onClick={() => setCount(1)}>Reset</button>
      </div>
      <div>
        <button>ORDER</button>
      </div>
    </>
  );
}

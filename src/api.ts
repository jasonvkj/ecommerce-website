let appProductionOrigins = ["https://urpey.xyz", "https://www.urpey.xyz"];

let is_production_env = appProductionOrigins.includes(window.location.origin);

export let API_ORIGIN: string = is_production_env
  ? "https://localhost:3000"
  : "http://localhost:8100";

export const shoppingCartItems = {
  products: [
    {
      _id: "268d5e2d-e3ef-46ec-830a-af8978c66b15",
      images: [
        {
          asset: {
            url: "https://cdn.sanity.io/images/0ze0a7fu/production/0ac1efe23ca8f3700632848961ca501597c9fae3-1800x2200.png",
          },
        },
      ],
      price: 1680,
      sku: "BP00204",
      slug: {
        _type: "slug",
        current: "chronograph-first/rose-gold",
      },
      title: "Chronograph First Rose Gold",
    },
    {
        _id: "268d5e2d-e3ef-46ec-830a-af8978c66b15",
        images: [
          {
            asset: {
              url: "https://cdn.sanity.io/images/0ze0a7fu/production/0ac1efe23ca8f3700632848961ca501597c9fae3-1800x2200.png",
            },
          },
        ],
        price: 1680,
        sku: "BP00204",
        slug: {
          _type: "slug",
          current: "chronograph-first/rose-gold",
        },
        title: "Chronograph First Rose Gold",
      },
      {
        _id: "268d5e2d-e3ef-46ec-830a-af8978c66b15",
        images: [
          {
            asset: {
              url: "https://cdn.sanity.io/images/0ze0a7fu/production/0ac1efe23ca8f3700632848961ca501597c9fae3-1800x2200.png",
            },
          },
        ],
        price: 1680,
        sku: "BP00204",
        slug: {
          _type: "slug",
          current: "chronograph-first/rose-gold",
        },
        title: "Chronograph First Rose Gold",
      },
      {
        _id: "268d5e2d-e3ef-46ec-830a-af8978c66b15",
        images: [
          {
            asset: {
              url: "https://cdn.sanity.io/images/0ze0a7fu/production/0ac1efe23ca8f3700632848961ca501597c9fae3-1800x2200.png",
            },
          },
        ],
        price: 1680,
        sku: "BP00204",
        slug: {
          _type: "slug",
          current: "chronograph-first/rose-gold",
        },
        title: "Chronograph First Rose Gold",
      },
  ],
};

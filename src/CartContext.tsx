import { createContext, useContext, useEffect, useState } from "react";
import { CartItem, Product } from "./types";

interface CartContextValue {
    cartItems: CartItem[];
    addToCart: (products: Product[]) => void;
    removeFromCart: (productId: string) => void;
    updateCartItemQuantity: (productId: string, quantity: number) => void;
    cartTotal: number;
    cartCount: number;
}

const CartContext = createContext<CartContextValue>({
    cartItems: [],
    addToCart: () => { },
    removeFromCart: () => { },
    updateCartItemQuantity: () => { },
    cartTotal: 0,
    cartCount: 0,
});

export const useCart = () => {
    return useContext(CartContext);
};

interface Props {
    children: React.ReactNode;
}

export const CartProvider = ({ children }: Props) => {
    const [cartItems, setCartItems] = useState<CartItem[]>([]);

    useEffect(() => {
        const cartItem = JSON.parse(localStorage.getItem("cartItems") || "{}");
        if (cartItem) {
            setCartItems(cartItem);
        }
    }, []);

    useEffect(() => {
        if (cartItems !== null)
            localStorage.setItem("cartItems", JSON.stringify(cartItems));
    }, [cartItems]);

    const addToCart = (products: Product[]) => {
        const existingCartItemIndex = cartItems.findIndex(
            (item) => item.products[0]._id === products[0]._id
        );
        if (existingCartItemIndex !== -1) {
            const existingCartItem = cartItems[existingCartItemIndex];
            const updatedCartItem = {
                ...existingCartItem,
                quantity: existingCartItem.quantity + 1,
            };
            const updatedCartItems = [...cartItems];
            updatedCartItems[existingCartItemIndex] = updatedCartItem;

            //console.log(existingCartItemIndex, updatedCartItems);
            setCartItems(updatedCartItems);
        } else {
            setCartItems([...cartItems, { products, quantity: 1 }]);
        }
    };

    const removeFromCart = (productId: string) => {
        const updatedCartItems = cartItems.filter(
            (item) => item.products[0]._id !== productId
        );
        setCartItems(updatedCartItems);
    };

    const updateCartItemQuantity = (productId: string, quantity: number) => {
        const existingCartItemIndex = cartItems.findIndex(
            (item) => item.products[0]._id === productId
        );
        if (existingCartItemIndex !== -1) {
            const existingCartItem = cartItems[existingCartItemIndex];
            const updatedCartItem = {
                ...existingCartItem,
                quantity,
            };
            const updatedCartItems = [...cartItems];
            updatedCartItems[existingCartItemIndex] = updatedCartItem;
            setCartItems(updatedCartItems);
        }
    };

    const cartTotal = cartItems.reduce(
        (total, item) => total + item.products[0].price * item.quantity,
        0
    );

    const cartCount = cartItems.reduce((count, item) => count + item.quantity, 0);

    return (
        <CartContext.Provider
            value={{
                cartItems,
                addToCart,
                removeFromCart,
                updateCartItemQuantity,
                cartTotal,
                cartCount,
            }}
        >
            {children}
        </CartContext.Provider>
    );
};
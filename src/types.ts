export interface MenuItems {
  name: string;
  path: string;
}

export interface CollectionList {
  model: string;
  path: string;
  image?: any;
  price?: string;
}

export interface AccountTabItems {
  name: string;
  path: string;
  image: any;
}

export interface ProductList {
  name: string;
  path: string;
  image: any;
}

export interface Category {
  _id: string;
  _createdAt: string;
  _updatedAt: string;
  _rev: string;
  _type: "category";
  slug: {
    _type: "slug";
    current: string;
  };
  title: string;
}

export interface Image {
  _key: string;
  _type: "image";
  asset: {
    url: string;
  };
}

export interface Product {
  _id: string;
  _updatedAt: string;
  _rev: string;
  _type: "product";
  title: string;
  price: number;
  slug: {
    _type: "slug";
    current: string;
  };
  description: string;
  category: {
    _type: "reference";
    _ref: string;
  };
  images: Image[];
  colour:string;
}

export type CartItem = {
  products: Product[];
  quantity: number;
};

export interface StripeProduct {
  id: string;
  amount_discount: number;
  amount_subtotal: number;
  amount_tax: number;
  amount_total: number;
  currency: string;
  description: string;
  object: string;
  quantity: number;
  price: {
    unit_amount: number;
  };
}
